package com.ruyuan.seckill.service.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.seckill.cache.Cache;
import com.ruyuan.seckill.domain.GoodsDO;
import com.ruyuan.seckill.domain.GoodsSkuDO;
import com.ruyuan.seckill.domain.enums.CachePrefix;
import com.ruyuan.seckill.domain.vo.CacheGoods;
import com.ruyuan.seckill.domain.vo.GoodsSkuVO;
import com.ruyuan.seckill.service.GoodsSkuManager;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 商品sku业务类
 * sku表通过hashcode字段来确定是否有规格变化
 * 通过lua脚本来更新库存
 */
@Service
public class GoodsSkuManagerImpl implements GoodsSkuManager {
    @Autowired
    private Cache cache;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 缓存中查询sku信息
     *
     * @param skuId
     * @return
     */
    @Override
    public GoodsSkuVO getSkuFromCache(Integer skuId) {
        // 从缓存读取sku信息
        String skuStr = stringRedisTemplate.opsForValue().get(CachePrefix.SKU.getPrefix() + skuId);
        return JSON.parseObject(skuStr,GoodsSkuVO.class) ;

    }

    /**
     * 查询sku信息
     *
     * @param skuId
     * @return
     */
    @Override
    public GoodsSkuVO getSku(Integer skuId) {
        GoodsSkuDO sku = (GoodsSkuDO) this.cache.get(CachePrefix.SKU.getPrefix() + skuId);
        if (sku == null) {
            sku = new GoodsSkuDO();
            sku.setCategoryId(1);
            sku.setCost(50D);
            sku.setEnableQuantity(100000);
            sku.setGoodsId(1);
            sku.setGoodsName("测试商品");
            sku.setPrice(9.8D);
            sku.setSellerId(1);
            sku.setQuantity(100000);
            sku.setSellerName("平台自营");
            sku.setSkuId(skuId);
            sku.setSn("00042");
            sku.setSpecs(null);
            sku.setWeight(312D);
            sku.setHashCode(-1);
            cache.put(CachePrefix.SKU.getPrefix() + skuId, sku);
        }
        GoodsSkuVO skuVo = new GoodsSkuVO();
        BeanUtils.copyProperties(sku, skuVo);

        //以下信息由商品中获取
        GoodsDO goodsDO = (GoodsDO) cache.get(CachePrefix.GOODS.getPrefix() + sku.getGoodsId());
        if(goodsDO==null){
           goodsDO=new GoodsDO();
            goodsDO.setGoodsId(1);
            goodsDO.setGoodsName("百草味菠萝干100g 菠萝块凤梨干/片蜜饯水果干小吃");
            goodsDO.setGoodsType("NORMAL");
            goodsDO.setSn("0098");
            goodsDO.setBrandId(0);
            goodsDO.setCategoryId(1);
            goodsDO.setGoodsType("NORMAL");
            goodsDO.setWeight(50D);
            goodsDO.setMarketEnable(1);
            goodsDO.setPrice(25.90);
            goodsDO.setCost(20D);
            goodsDO.setMktprice(25.90);
            goodsDO.setGoodsTransfeeCharge(1);
            goodsDO.setCreateTime(1497358581L);
            goodsDO.setLastModify(1516259987L);
            goodsDO.setSellerId(1);
            goodsDO.setDisabled(1);
            goodsDO.setQuantity(100000);
            goodsDO.setEnableQuantity(100000);
            goodsDO.setShopCatId(1);
            goodsDO.setTemplateId(0);
            goodsDO.setSellerName("平台自营");
            goodsDO.setIsAuth(1);
            cache.put(CachePrefix.GOODS.getPrefix() + sku.getGoodsId(),goodsDO);

        }
        CacheGoods cacheGoods = new CacheGoods();
        goodsDO.setIsAuth(1);
        BeanUtils.copyProperties(goodsDO,cacheGoods);
        cacheGoods.setSkuList(Arrays.asList(skuVo));
        cache.put(CachePrefix.CACHE_GOODS.getPrefix() + sku.getGoodsId(),cacheGoods);

        skuVo.setLastModify(goodsDO.getLastModify());
        skuVo.setGoodsTransfeeCharge(goodsDO.getGoodsTransfeeCharge());
        skuVo.setDisabled(goodsDO.getDisabled());
        skuVo.setMarketEnable(goodsDO.getMarketEnable());
        skuVo.setTemplateId(goodsDO.getTemplateId());
        skuVo.setGoodsType(goodsDO.getGoodsType());
        //如果sku绑定的运费模板不为空的话则将script重新赋值
        if (!skuVo.getTemplateId().equals(0)) {
            List<String> scripts = (List<String>) cache.get(CachePrefix.SHIP_SCRIPT.getPrefix() + skuVo.getTemplateId());
            if (scripts != null) {
                skuVo.setScripts(scripts);
            }
        }
        cache.put(CachePrefix.SKU.getPrefix() + skuId, skuVo);
        return skuVo;
    }


}
