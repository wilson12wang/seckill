package com.ruyuan.seckill.service.cartbuilder;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.vo.CartVO;

import java.util.List;

/**
 * 运费计算器
 */
public interface CartShipPriceCalculator  {

    void countShipPrice(List<CartVO>cartList) ;
    void countShipPrice(List<CartVO>cartList, Buyer buyer) ;

}
