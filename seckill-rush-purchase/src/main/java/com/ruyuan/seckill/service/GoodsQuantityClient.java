package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.vo.GoodsQuantityVO;

import java.util.List;

/**
 * 商品库存操作客户端
 *
 * 统一为一个接口（更新接口）<br/>
 * 内部实现为redis +lua 保证原子性
 */
public interface GoodsQuantityClient {


    /**
     * 秒杀库存更新接口
     *
     * @param goodsQuantityList 要更新的库存vo List
     * @return 如果更新成功返回真，否则返回假
     */
    Boolean updateSeckillSkuQuantity(List<GoodsQuantityVO> goodsQuantityList);


}


