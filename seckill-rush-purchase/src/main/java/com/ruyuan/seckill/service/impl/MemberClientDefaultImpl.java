package com.ruyuan.seckill.service.impl;

import com.ruyuan.seckill.cache.Cache;
import com.ruyuan.seckill.domain.Member;
import com.ruyuan.seckill.service.MemberClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员业务默认实现
 */
@Service
public class MemberClientDefaultImpl implements MemberClient {

    @Autowired
    private Cache cache;
    @Override
    public Member getModel(Integer memberId) {
        return (Member) cache.get(memberId);
    }

}
