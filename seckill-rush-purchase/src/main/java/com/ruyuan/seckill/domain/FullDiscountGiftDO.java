package com.ruyuan.seckill.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class FullDiscountGiftDO implements Serializable {

    private static final long serialVersionUID = 953152013470095L;

    /**
     * 赠品id
     */
    @ApiModelProperty(hidden = true)
    private Integer giftId;

    /**
     * 赠品名称
     */
    @ApiModelProperty(name = "gift_name", value = "赠品名称", required = false)
    private String giftName;

    /**
     * 赠品金额
     */
    @ApiModelProperty(name = "gift_price", value = "赠品金额", required = false)
    private Double giftPrice;

    /**
     * 赠品图片
     */
    @NotEmpty(message = "请上传赠品图片")
    @ApiModelProperty(name = "gift_img", value = "赠品图片", required = false)
    private String giftImg;

    /**
     * 库存
     */
    @NotNull(message = "请填写库存")
    @ApiModelProperty(name = "actual_store", value = "库存", required = false)
    private Integer actualStore;

    /**
     * 赠品类型
     */
    @ApiModelProperty(name = "gift_type", value = "赠品类型", required = false)
    private Integer giftType;

    /**
     * 可用库存
     */
    @ApiModelProperty(name = "enable_store", value = "可用库存", required = false)
    private Integer enableStore;

    /**
     * 活动时间
     */
    @ApiModelProperty(name = "create_time", value = "活动时间", required = false)
    private Long createTime;

    /**
     * 活动商品id
     */
    @ApiModelProperty(name = "goods_id", value = "活动商品id", required = false)
    private Integer goodsId;

    /**
     * 是否禁用
     */
    @ApiModelProperty(name = "disabled", value = "是否禁用", required = false)
    private Integer disabled;

    /**
     * 店铺id
     */
    @ApiModelProperty(name = "seller_id", value = "商家id", required = false)
    private Integer sellerId;

    public Integer getGiftId() {
        return giftId;
    }

    public void setGiftId(Integer giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public Double getGiftPrice() {
        return giftPrice;
    }

    public void setGiftPrice(Double giftPrice) {
        this.giftPrice = giftPrice;
    }

    public String getGiftImg() {
        return giftImg;
    }

    public void setGiftImg(String giftImg) {
        this.giftImg = giftImg;
    }

    public Integer getGiftType() {
        return giftType;
    }

    public void setGiftType(Integer giftType) {
        this.giftType = giftType;
    }

    public Integer getActualStore() {
        return actualStore;
    }

    public void setActualStore(Integer actualStore) {
        this.actualStore = actualStore;
    }

    public Integer getEnableStore() {
        return enableStore;
    }

    public void setEnableStore(Integer enableStore) {
        this.enableStore = enableStore;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }


}
